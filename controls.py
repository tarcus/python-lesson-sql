import sqlite3


class UsersControl:
    def __init__(self, dbname=None):
        self.connection = sqlite3.connect(dbname) if dbname else None
        self.cursor = self.connection.cursor() if dbname else None

    def match_by_name(self, name):
        query = (
            f'''
                SELECT * FROM users
                WHERE name LIKE '{name[0]}%{name[1:]}%'
            '''
        )
        return self.connection.execute(query).fetchall()

    def match_by_age(self, age):
        query = (
            f'''
                SELECT * FROM users
                WHERE age = {age}
            '''
        )
        return self.connection.execute(query).fetchall()

    def add_user(self, user_data):
        query = '''
            INSERT INTO users
            (name, surname, age, email, mobile)
            values(?,?,?,?,?)
        '''
        self.connection.execute(query, user_data)
        self.connection.commit()


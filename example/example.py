import sqlite3
import pprint as p

query = '''CREATE TABLE users
            (id integer primary key
            , name varchar(100) NOT NULL
            , surname varchar(100) NOT NULL
            , age integer
            , email varchar(100)
            , mobile integer
            )
'''
conn = sqlite3.connect('test.db')
cursor = conn.cursor()
#cursor.execute(query)

query_add_users = '''
    INSERT INTO users
    (name, surname, age, email, mobile)
    values(?,?,?,?,?)
'''
users = [
    ('Hermiona', 'Granger', 20, 'granger@hogwarts.edu', 668899),
    ('Rhon', 'Wizly', 20, 'Wizly@hogwarts.edu', None),
    ('Albus', 'D', 150, 'granger@hogwarts.edu', 123)
]
#cursor.executemany(query_add_users, users)
#conn.commit()

query = '''
    SELECT name from users
'''

data = conn.execute(query).fetchall()
p.pprint(data)
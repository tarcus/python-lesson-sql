import tkinter as tk


class AddUserForm(tk.Frame):
    def __init__(self, master=None, service=None):
        super().__init__(master)
        self.service = service
        self.inputs = []
        self.pack()
        self.render_form(master)

    def render_form(self, master):
        form = tk.Frame(master)
        form.pack()
        fields = ('name', 'surname', 'age', 'email', 'mobile')
        for field in fields:
            self.inputs.append(tk.StringVar())
            frame = tk.Frame(form)
            frame.pack()
            field_label = tk.Label(frame, text=field)
            field_label.pack(side=tk.LEFT)
            field_input = tk.Entry(frame, textvariable=self.inputs[-1])
            field_input.pack(side=tk.RIGHT)
        btn_add_user = tk.Button(form, text='Add user', command=self.add_user)
        btn_add_user.pack(side=tk.BOTTOM)

    def add_user(self):
        user_data = tuple(map(lambda x: x.get(), self.inputs))
        self.service.add_user(user_data)
        self.destroy()


class SearchForm(tk.Frame):
    def __init__(self, master=None, service=None):
        super().__init__(master)
        self.service = service
        self.pack()
        self.render_form()
        self.result_field = self.render_result([])

    def render_result(self, result):
        field = tk.Frame(self)
        field.pack()
        for user in result:
            user_str = f'name: {user[1]}; surname: {user[2]}; age: {user[3]}'
            row = tk.Label(field, text=user_str)
            row.pack()
        return field

    def render_form(self):
        form = tk.Frame(self)
        form.pack()
        input_search = tk.Entry(form)
        input_search.pack(side=tk.RIGHT)
        btn_search = tk.Button(form, text='Search', command=lambda: self.search(input_search))
        btn_search.pack(side=tk.RIGHT)
        bottom_frame = tk.Frame(self)
        bottom_frame.pack(side=tk.BOTTOM)
        btn_add_user = tk.Button(bottom_frame, text='Add user', command=self.show_add_user_form)
        btn_add_user.pack()

    def show_add_user_form(self):
        form = tk.Toplevel()
        AddUserForm(form, self.service)
        form.transient(self.master)
        form.grab_set()
        form.focus_set()
        form.wait_window()

    def search(self, input_search):
        query = input_search.get()
        result = self.service.match_by_name(query)
        self.result_field.destroy()
        self.result_field = self.render_result(result)

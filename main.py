import tkinter as tk
from form import SearchForm
from controls import UsersControl

root = tk.Tk()
root.title("search form")
root.geometry("300x400")

control = UsersControl('test.db')
search_form = SearchForm(root, control)

root.mainloop()
